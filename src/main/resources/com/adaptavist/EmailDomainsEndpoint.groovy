package com.adaptavist

import com.onresolve.scriptrunner.canned.common.admin.ScriptRunnerTestRunner
import groovy.transform.BaseScript
import groovy.json.JsonBuilder
import org.junit.runner.RunWith

@RunWith(ScriptRunnerTestRunner)
class emailDomainsEndpoints {
    def endpoints(
            httpMethod

    : "GET", groups: [ "jira-administrators" ] ) {
        def email = "joshhilger@gmail.com".split('@')
        return Response.ok(new JsonBuilder([email]).vtoString()).build()

    }
}
'''
def emailEndpoints = "joshhilger@gmail.com".split('@')
println(emailEndpoints[1])
'''